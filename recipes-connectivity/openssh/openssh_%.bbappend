FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI_append = "file://sshd.run"

RUNIT_SERVICE = "sshd.run"
RUNIT_AUTO_ENABLE = "enable"
