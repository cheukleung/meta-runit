FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI_append = "file://dbus.run"

RUNIT_SERVICE = "dbus.run"
