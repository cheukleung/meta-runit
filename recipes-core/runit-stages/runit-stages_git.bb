SUMMARY = "runit stage and base services"
LIC_FILES_CHKSUM = "file://COPYING;md5=976cd9d5dfe0f556e3793bbd954dc7f4"
LICENSE = "PD"

SRC_URI = "git://gitlab.com/rehman.anees/runit-stages.git;protocol=https"

SRCREV = "95e6b9c8b7ba60767cad800c5cbe2284c8a9c7ef"

S = "${WORKDIR}/git"

FILES_${PN} += "${sysconfdir}"

EXTRA_OEMAKE = 'DESTDIR="${D}"'

do_install () {
    oe_runmake all
}
