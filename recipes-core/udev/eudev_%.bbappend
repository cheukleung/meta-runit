FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI_append = "file://udevd.run"

RUNIT_SERVICE = "udevd.run"
