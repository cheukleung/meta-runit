FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI_append = "file://rngd.run"

RUNIT_SERVICE = "rngd.run"
